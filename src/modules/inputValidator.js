export const signUpBodyValidate = (requestBody) => {
  const hasName = Boolean(requestBody.name);
  const hasEmail = Boolean(requestBody.email);
  const hasUsername = Boolean(requestBody.username);
  const hasPassword = Boolean(requestBody.password);

  const hasEmptyField = Object.keys(requestBody).some(
    (property) => property.length === 0
  );

  return !hasEmptyField && hasName && hasEmail && hasUsername && hasPassword;
};

export const loginBodyValidate = (requestBody) => {
  const hasEmail = Boolean(requestBody.email);
  const hasPassword = Boolean(requestBody.password);

  const hasEmptyField = Object.keys(requestBody).some(
    (property) => property.length === 0
  );

  return !hasEmptyField && hasEmail && hasPassword;
};
