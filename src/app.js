import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { v4 as uuid } from 'uuid';
import cookieParser from 'cookie-parser';

import {
  signUpBodyValidate,
  loginBodyValidate,
} from './modules/inputValidator';
import keystore from './secrets';

export const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser(keystore.get('symetric_cookie_sig').k));

app.disable('x-powered-by');

app.use(function(req, res, next) {
  // Need these headers to allow the signup process to work
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

export async function setupApp({ db, broker }) {
  app.get('/user', cors(), async (_, res) => {
    const userList = await db.collection('user').find({}).toArray();

    res.json({ userList });
  });

  app.post('/user', cors(), (req, res) => {
    const isInputValid = signUpBodyValidate(req.body);

    if (!isInputValid) {
      res.status(400).json({ message: 'Bad user input' });
      return;
    }

    const { name, password, username, email } = req.body;

    const user = {
      uuid: uuid(),
      email,
      password,
      name,
      username,
    };

    broker.publish('user', JSON.stringify(user));

    res.json({ user });
  });

  app.options(
    '/user/login',
    cors({ origin: 'http://localhost:8080', credentials: true })
  );
  app.post(
    '/user/login',
    cors({ origin: 'http://localhost:8080', credentials: true }),
    async (req, res) => {
      const isInputValid = loginBodyValidate(req.body);

      if (!isInputValid) {
        res.status(400).json({ message: 'Bad user input' });
        return;
      }

      const { email, password } = req.body;

      const userFromDB = await db.collection('user').findOne({ email });

      if (!userFromDB) {
        res
          .status(400)
          .json({ error: `User with email "${email}" does not exist.` });
        return;
      }

      const isCorrectPassword =
        String(userFromDB.password) === String(password);

      if (!isCorrectPassword) {
        res.status(400).json({ error: `Wrong password for user "${email}".` });
        return;
      }

      res.cookie('user_auth_token', '1729', { signed: true });
      res.json({ message: 'Login successful' });
    }
  );
}
