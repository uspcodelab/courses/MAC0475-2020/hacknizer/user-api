import { connect } from 'node-nats-streaming';

const { BROKER_CLUSTER_ID, BROKER_URL } = process.env;

export const stan = connect(BROKER_CLUSTER_ID, 'user-api', {
  url: BROKER_URL,
});
