import fs from 'fs';
import jose from 'jose';

const API_SECRETS_PATH = './src/secrets/development';

const keystore = new jose.JWKS.KeyStore([
  jose.JWK.asKey(
    fs.readFileSync(`${API_SECRETS_PATH}/symetric_cookie_sig_key.pem`),
    {
      alg: 'HS256',
      kid: 'symetric_cookie_sig',
      use: 'sig',
    }
  ),
]);

export default keystore;
